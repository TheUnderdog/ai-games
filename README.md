#Comment to stop GitLab deleting this stuff


# AI-Games

Games produced by an AI, reviewed by a human.

# Disclaimer

No guarantee of merchantability or fitness. No warranty. User assumes all risks in running any of the code and/or files found here. Review process only checks to see if code might run, and is not a guarantee of safety or fitness of said code. No warranty, express or implied, is granted in the use of this work.

# Copyright

All code found here is released under MIT licence. Images and sounds used may be subject to copyright and are the copyrights of their respective owner(s). Where possible, copyleft licenced material has been used, but is purely for demonstration purposes and should be replaced when using the code.
