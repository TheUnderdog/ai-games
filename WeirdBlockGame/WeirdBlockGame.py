#Make a very basic Sonic the Hedgehog game in PyGame.

import sys, pygame, random, time

pygame.init()

#Display screen size
size = width, height = 600, 600

#music
pygame.mixer.init()
#pygame.mixer.music.load("song.mp3")
#pygame.mixer.music.play()

#colors
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)

#clock
clock = pygame.time.Clock()

#font
myfont = pygame.font.SysFont("monospace", 20)

#screen
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Sonic Game")

#sonic
sonic = pygame.image.load("sonic.png")
sonic = pygame.transform.scale(sonic, (40,40))

#enemy
enemy = pygame.image.load("enemy.png")
enemy = pygame.transform.scale(enemy, (40,40))

#starting position
sonic_position = sonic.get_rect()
sonic_position = sonic_position.move(300, 600)
enemy_position = enemy.get_rect()
enemy_position = enemy_position.move(0,0)

#movement
movement = 15

#starting direction of sonic
direction = "right"

#increasing speed
level = 1

#score
score = 0

#when game is over
gameover = "no"

#variables for game over
final_score = 0
final_time = 0
final_enemy = 0

#variable for text
text = ""

#variable for time
time = 0

#font
font = pygame.font.SysFont("bauhaus93", 36)

#start screen
def start_screen():
    screen.fill(black)
    text = font.render("Welcome to Sonic Game!", True, white)
    screen.blit(text, [200, 300])
    text = font.render("Press space to continue", True, white)
    screen.blit(text, [200, 400])
    pygame.display.flip()

#game over screen
def game_over():
    screen.fill(black)
    text1 = font.render("GAME OVER", True, red)
    screen.blit(text1, [200, 300])
    text2 = font.render("Your score is " + str(final_score), True, white)
    screen.blit(text2, [200, 400])
    text2 = font.render("Time " + str(final_time), True, white)
    screen.blit(text2, [200, 450])
    text3 = font.render("Enemy " + str(final_enemy), True, white)
    screen.blit(text3, [200, 500])
    pygame.display.flip()

#starting screen
start_screen()

start_time = clock.get_time()

#game loop
while True:
    for event in pygame.event.get():
        #quit
        if event.type == pygame.QUIT: sys.exit()
        #key presses
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                direction = "right"
            if event.key == pygame.K_LEFT:
                direction = "left"
            if event.key == pygame.K_UP:
                direction = "up"
            if event.key == pygame.K_DOWN:
                direction = "down"
            if event.key == pygame.K_SPACE:
                #start time
                start_time = time.time()

    #movement
    if direction == "right":
        sonic_position = sonic_position.move(movement, 0)
        if sonic_position.right > width:
            sonic_position = sonic_position.move(-width, 0)
    if direction == "left":
        sonic_position = sonic_position.move(-movement, 0)
        if sonic_position.left < 0:
            sonic_position = sonic_position.move(width, 0)
    if direction == "up":
        sonic_position = sonic_position.move(0, -movement)
        if sonic_position.top < 0:
            sonic_position = sonic_position.move(0, height)
    if direction == "down":
        sonic_position = sonic_position.move(0, movement)
        if sonic_position.bottom > height:
            sonic_position = sonic_position.move(0, -height)

    #draw background
    screen.fill(black)
    screen.blit(sonic, sonic_position)
    screen.blit(enemy, enemy_position)

    #game over
    if pygame.Rect.colliderect(sonic_position, enemy_position):
        gameover = "yes"

    #time
    current_time = (clock.get_time() - start_time)
    time = myfont.render(str(int(current_time)), True, white)
    screen.blit(time, [0,0])

    #score
    text = myfont.render(str(score), True, white)
    screen.blit(text, [300, 0])

    #level
    text = myfont.render(str(level), True, white)
    screen.blit(text, [500, 0])

    #collision
    if pygame.Rect.colliderect(sonic_position, enemy_position):
        #increase speed
        movement = movement + 2
        #increase level
        level = level + 1
        #generate new enemy
        enemy_position = enemy_position.move(width, 0)
        enemy_position = enemy_position.move(-width, 0)
        #increase score
        score = score + 1
        #update score
        text = myfont.render(str(score), True, white)
        screen.blit(text, [300, 0])

    #generate new enemy
    if enemy_position.right > width:
        enemy_position = enemy_position.move(-width, 0)
        enemy_position = enemy_position.move(-width, 0)
        #increase score
        score = score + 1
        #update score
        text = myfont.render(str(score), True, white)
        screen.blit(text, [300, 0])

    #game over
    if gameover == "yes":
        gameover = "no"
        final_score = score
        final_time = int(current_time)
        final_enemy = enemy_position.left
        game_over()

    pygame.display.flip()
    clock.tick(60)
