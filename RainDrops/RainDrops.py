#Make a very basic Sonic the Hedgehog game in PyGame.

import pygame
import random
import sys

pygame.init()

WIDTH = 800
HEIGHT = 600

RED = (255, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
BACKGROUND_COLOR = (0, 0, 0)

playerSize = 50
playerPos = [WIDTH/2, HEIGHT-2*playerSize]

enemySize = 50
enemyPos = [random.randint(0, WIDTH-enemySize), 0]
enemyList = [enemyPos]

SPEED = 10

screen = pygame.display.set_mode((WIDTH, HEIGHT))

gameOver = False

score = 0

clock = pygame.time.Clock()

myFont = pygame.font.SysFont("monospace", 35)

def set_level(score, SPEED):
	if score < 20:
		SPEED = 5
	elif score < 40:
		SPEED = 8
	elif score < 60:
		SPEED = 12
	else:
		SPEED = 15
	return SPEED
	# SPEED = score/5 + 1

def drop_enemies(enemyList):
	delay = random.random()
	if len(enemyList) < 10 and delay < 0.1:
		x_pos = random.randint(0, WIDTH-enemySize)
		y_pos = 0
		enemyList.append([x_pos, y_pos])

def draw_enemies(enemyList):
	for enemyPos in enemyList:
		pygame.draw.rect(screen, BLUE, (enemyPos[0], enemyPos[1], enemySize, enemySize))

def update_enemy_positions(enemyList, score):
	for idx, enemyPos in enumerate(enemyList):
		if enemyPos[1] >= 0 and enemyPos[1] < HEIGHT:
			enemyPos[1] += SPEED
		else:
			enemyList.pop(idx)
			score += 1
	return score

def collision_check(enemyList, playerPos):
	for enemyPos in enemyList:
		if detect_collision(enemyPos, playerPos):
			return True
	return False

def detect_collision(playerPos, enemyPos):
	p_x = playerPos[0]
	p_y = playerPos[1]

	e_x = enemyPos[0]
	e_y = enemyPos[1]

	if (e_x >= p_x and e_x < (p_x + playerSize)) or (p_x >= e_x and p_x < (e_x+enemySize)):
		if (e_y >= p_y and e_y < (p_y + playerSize)) or (p_y >= e_y and p_y < (e_y+enemySize)):
			return True
	return False

while not gameOver:

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()

		if event.type == pygame.KEYDOWN:

			x = playerPos[0]
			y = playerPos[1]

			if event.key == pygame.K_LEFT:
				x -= playerSize
			elif event.key == pygame.K_RIGHT:
				x += playerSize

			playerPos = [x,y]

	screen.fill(BACKGROUND_COLOR)

	drop_enemies(enemyList)
	score = update_enemy_positions(enemyList, score)
	SPEED = set_level(score, SPEED)

	text = "Score:" + str(score)
	label = myFont.render(text, 1, YELLOW)
	screen.blit(label, (WIDTH-200, HEIGHT-40))

	if collision_check(enemyList, playerPos):
		gameOver = True
		break

	draw_enemies(enemyList)

	pygame.draw.rect(screen, RED, (playerPos[0], playerPos[1], playerSize, playerSize))

	clock.tick(30)

	pygame.display.update()
