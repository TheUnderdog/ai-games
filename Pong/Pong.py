#Make a pong game in PyGame.

import pygame
from random import randint
import time

pygame.init()
pygame.font.init()

#Define Colors
white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)

#Paddle Speed
paddle_speed = 0.5

#Window Size
display_width = 800
display_height = 600

#Ball Speed
ball_x = display_width/2
ball_y = randint(0, display_height)
ball_speed_x = 0.2
ball_speed_y = 0.2

#Game Window
gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption("Pong")

#Tick Rate
tick_rate = 60

#Paddle
paddle_width = 10
paddle_height = 100

paddle_x = 0
paddle_y_l = 0
paddle_y_r = 0
paddle_move = 0

#Score
r_score = 0
l_score = 0

#Text
font = pygame.font.SysFont("comicsansms", 35)

#Game Start
def game_start():
	gameDisplay.fill(white)
	game_loop()

#Game Loop
def game_loop():
	global ball_speed_x, ball_speed_y, ball_x, ball_y, paddle_speed, paddle_move, paddle_y_l, paddle_x_l, paddle_y_r, paddle_x_r, display_width, display_height, r_score, l_score, font

	#Ball Reset
	def ball_reset():
		global ball_x, ball_y, ball_speed_x, ball_speed_y
		ball_x = display_width/2 #randint(0, display_width)
		ball_y = randint(0, display_height)
		ball_speed_x = 0.2
		ball_speed_y = 0.2

	#Paddles
	def paddle_r_up():
		global paddle_speed, paddle_y_r
		paddle_y_r -= paddle_speed
	
	def paddle_r_down():
		global paddle_speed, paddle_y_r
		paddle_y_r += paddle_speed
		
	def paddle_l_up():
		global paddle_speed, paddle_y_l
		paddle_y_l -= paddle_speed
	
	def paddle_l_down():
		global paddle_speed, paddle_y_l
		paddle_y_l += paddle_speed

	#Paddle Reset
	def paddle_reset():
		global paddle_y_l, paddle_y_r
		paddle_y_l = 300
		paddle_y_r = 300

	#Ball Movement
	def ball_movement():
		global ball_x, ball_y, ball_speed_x, ball_speed_y, r_score, l_score, font
		ball_x += ball_speed_x
		ball_y += ball_speed_y
		
		if ball_x < 0:
			ball_reset()
			l_score += 1
			ball_speed_x = -ball_speed_x
		elif ball_x > display_width:
			ball_reset()
			r_score += 1
			ball_speed_x = -ball_speed_x
		if ball_y < 0:
			ball_speed_y = -ball_speed_y
			ball_y = 0
		elif ball_y > display_height:
			ball_speed_y = -ball_speed_y
			ball_y = display_height - 1

		#Ball-Paddle Collision
		if ball_y > paddle_y_l and ball_y < paddle_y_l + paddle_height:
			if ball_x <= paddle_width:
				ball_speed_x = -ball_speed_x
			
		if ball_y > paddle_y_r and ball_y < paddle_y_r + paddle_height:
			if ball_x >= (display_width - paddle_width):
				ball_speed_x = -ball_speed_x

	#Paddle Movement
	def paddle_movement():
		global paddle_y_r, paddle_y_l, paddle_height, display_height
		
		if paddle_y_l < 0:
			paddle_y_l = 0
		elif paddle_y_l + paddle_height > display_height:
			paddle_y_l = display_height - paddle_height
		
		if paddle_y_r < 0:
			paddle_y_r = 0
		elif paddle_y_r + paddle_height > display_height:
			paddle_y_r = display_height - paddle_height

	#Display Score
	def show_score():
		text = font.render(str(r_score), True, black)
		gameDisplay.blit(text, (0,0))
		text = font.render(str(l_score), True, black)
		gameDisplay.blit(text, (display_width - 50, 0))

	l_up = False
	l_down = False
	r_up = False
	r_down = False

	#Game Loop
	while True:
		#Event Loop
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_w:
					l_up = True
				elif event.key == pygame.K_s:
					l_down = True
				if event.key == pygame.K_UP:
					r_up = True
				elif event.key == pygame.K_DOWN:
					r_down = True
					
			elif event.type == pygame.KEYUP:
				if event.key == pygame.K_w:
					l_up = False
				elif event.key == pygame.K_s:
					l_down = False
				if event.key == pygame.K_UP:
					r_up = False
				elif event.key == pygame.K_DOWN:
					r_down = False

		if r_up:
			paddle_r_up()
		elif r_down:
			paddle_r_down()
		if l_up:
			paddle_l_up()
		elif l_down:
			paddle_l_down()

		#Background Color
		gameDisplay.fill(white)

		#Paddle Movement
		paddle_movement()
		paddle_r = pygame.draw.rect(gameDisplay, blue, [display_width - paddle_width, paddle_y_r, paddle_width, paddle_height]) 
		paddle_l = pygame.draw.rect(gameDisplay, red, [paddle_x, paddle_y_l, paddle_width, paddle_height])

		#Ball Movement
		ball_movement()
		ball = pygame.draw.circle(gameDisplay, black, [ball_x, ball_y], 10)

		#Show Score
		show_score()

		#Update PyGame Frame
		pygame.display.update()

		#Game Tick
		#clock.tick(tick_rate)

#Run Game
game_start()

#Quit Game
pygame.quit()
quit()
