Flaws reviewed and corrected:

AI made controls move both paddles simultaneously. Fixed to independent.
Ball paddle hit detection was fixed to one paddle only. Fixed to indepdent.
Right and Left paddles were on the wrong side of the screen. Swapped.
No independent paddle y for left and right. Left and right y added.
Garish colour (bright green!) for the 'ball'. Changed to black.
Ball size absurdly too big. Reduced in size.
Up/Down, W/S keys had to be manually pressed to move paddles. Fixed to continuous-hold.
Ball started off-screen giving a point to right.
Randomised ball's x position with a high probability it'd start at an impossible to respond to location. Normalised to display_width divided by 2.
Ball speed too fast at -1. Changed to 0.2.
Paddle speed too fast. Changed to 0.2.
Scores were the wrong way around. Swapped.

Flaws reviewed but not corrected:

Game lasts until infinity. Can just close window to leave.
